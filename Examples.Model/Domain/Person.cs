﻿using System;

namespace Examples.Model.Domain
{
    public sealed class Person : ICloneable, IEquatable<Person>
    {
        public Person(string name, string surname, string email = "")
        {
            FirstName = name;
            LastName = surname;
            Email = string.IsNullOrEmpty(email) ? $"{name}.{surname}@company.com".ToLower() : email;
        }

        public string Email { get; set; }
        public string Departament { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        internal Person Clone() => (Person)MemberwiseClone();
        
        internal void Update(Person person)
        {
            Email = person.Email;
            Departament = person.Departament;
            FirstName = person.FirstName;
            LastName = person.LastName;
        }

        object ICloneable.Clone() => Clone();

        bool IEquatable<Person>.Equals(Person other)
        {
            return Equals(other) ||
                Email == other.Email
                && FirstName == other.FirstName
                && LastName == other.LastName
                && Departament == other.Departament;
        }
    }
}
