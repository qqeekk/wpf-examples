﻿using Examples.Model.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Examples.Model.DataAccess
{
    internal static class PersonRepository
    {
        static PersonRepository()
        {
            People = new List<Person>
            {
                new Person("Anders", "Hejlsberg") { Departament = "C#" },
                new Person("Guido", "Van Rossum") { Departament = "Python" },
                new Person("James", "Gosling") { Departament = "Java" },
                new Person("Don", "Syme") { Departament = "F#" },
                new Person("Andrey", "Breslav") { Departament = "Kotlin" },
                new Person("Bjarne", "Stroustrup") { Departament = "C++" },
            };
        }
        
        public static ICollection<Person> People { get; }

        public static IEnumerable<string> Departaments =>
            People.Select(p => p.Departament).Distinct().ToList();
    }
}
