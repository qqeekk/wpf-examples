﻿using Examples.Model.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Examples.Model.UseCases
{
    public class GetDepartamentsCommand
    {
        public IEnumerable<string> Run() => PersonRepository.Departaments;
    }
}
