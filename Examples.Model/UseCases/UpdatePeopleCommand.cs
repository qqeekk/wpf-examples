﻿using Examples.Model.DataAccess;
using Examples.Model.Domain;
using Saritasa.Tools.Common.Utils;
using System.Collections.Generic;

namespace Examples.Model.UseCases
{
    public class UpdatePeopleCommand
    {
        public IEnumerable<Person> Updated { get; set; }

        public void Run()
        {
            var source = PersonRepository.People;

            var diff = CollectionUtils.Diff(source, Updated, (s, t) => s.Email == t.Email);
            CollectionUtils.ApplyDiff(source, diff, (s, t) => s.Update(t));
        }
    }
}
 