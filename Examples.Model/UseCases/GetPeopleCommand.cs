﻿using Examples.Model.DataAccess;
using Examples.Model.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Examples.Model.UseCases
{
    public class GetPeopleCommand
    {
        public IEnumerable<Person> Run() =>
            PersonRepository.People.Select(p => p.Clone()).ToList();
    }
}
