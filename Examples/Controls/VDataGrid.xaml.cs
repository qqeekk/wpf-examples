﻿using Examples.Model.UseCases;
using Examples.ViewModels;
using System.Linq;
using System.Windows.Controls;

namespace Examples.Controls
{
    /// <summary>
    /// Interaction logic for VDataGrid.xaml
    /// </summary>
    public partial class VDataGrid : Page
    {
        const string DepartamentsKey = nameof(DepartamentsKey);

        public VDataGrid()
        {
            var people = new GetPeopleCommand().Run()
                .Select(p => new PersonViewModel(p))
                .ToList();

            Resources[DepartamentsKey] = new GetDepartamentsCommand().Run();

            InitializeComponent();
            dgdPeople.ItemsSource = people;
            dgdPeopleEditable.ItemsSource = people;
        }
    }
}
