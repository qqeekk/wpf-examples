﻿using Examples.Model.UseCases;
using Examples.ViewModels;
using System.Linq;
using System.Windows.Controls;

namespace Examples.Controls
{
    /// <summary>
    /// Interaction logic for ItemsControl.xaml
    /// </summary>
    public partial class VItemsControl : Page
    {
        public VItemsControl()
        {
            var people = new GetPeopleCommand().Run()
                .Select(p => new PersonViewModel(p))
                .ToList();

            InitializeComponent();

            lbxPeople.ItemsSource = people;
            lvPeople.ItemsSource = people;
            cbxPeople.ItemsSource = new GetDepartamentsCommand().Run();
        }
    }
}
