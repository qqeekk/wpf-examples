﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Examples.Controls
{
    /// <summary>
    /// Interaction logic for VButton.xaml
    /// </summary>
    public partial class VContentControl : Page
    {
        private long clickCount = -1;

        public VContentControl()
        {
            InitializeComponent();
            UpdateRepeat();
            UpdateToggle();
        }

        private void btnWithHandler_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Button handler just worked");
        }

        private void RepeatButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateRepeat();
        }

        private void btnToggle_Click(object sender, RoutedEventArgs e)
        {
            UpdateToggle();
        }

        private void UpdateToggle()
        {
            btnToggle.Content = $"Toggle ({(btnToggle.IsChecked == true ? "pressed" : "released")})";
        }

        private void UpdateRepeat()
        {
            btnRepeat.Content = $"Press and hold ({++clickCount})";
        }
    }
}
