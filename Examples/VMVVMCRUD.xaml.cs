﻿using Examples.ViewModels;
using System.Windows.Controls;

namespace Examples
{
    /// <summary>
    /// Interaction logic for VMVVMCRUD.xaml
    /// </summary>
    public partial class VMVVMCRUD : Page
    {
        public VMVVMCRUD()
        {
            DataContext = new PersonCRUDViewModel();
            InitializeComponent();
        }
    }
}
