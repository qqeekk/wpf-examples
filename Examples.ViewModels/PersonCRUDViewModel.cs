﻿using Examples.Model.UseCases;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace Examples.ViewModels
{
    public class PersonCRUDViewModel: INotifyPropertyChanged
    {
        public ICollection<PersonViewModel> People { get; }
        public IEnumerable<string> Departaments { get; }
        public ICommand SynchronizeCommand { get; }

        public PersonCRUDViewModel()
        {
            var people = new GetPeopleCommand().Run()
                .Select(p => new PersonViewModel(p))
                .ToList();

            // We use binding list because it tracks item updates also.
            var peopleObservable = new BindingList<PersonViewModel>(people);
            var synchronizeCommand = new SynchronizeCommand();

            // Challenge button state update on each collection modification:
            // insert / remove / item update.
            peopleObservable.ListChanged += (s, e) =>
            {
                synchronizeCommand.ValidateExecutionState();
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(People)));
            };

            // Set properties.
            Departaments = new GetDepartamentsCommand().Run();
            People = peopleObservable;
            SynchronizeCommand = synchronizeCommand;
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
    }
}
