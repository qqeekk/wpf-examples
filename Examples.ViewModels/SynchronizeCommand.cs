﻿using Examples.Model.Domain;
using Examples.Model.UseCases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Examples.ViewModels
{
    internal class SynchronizeCommand : ICommand
    {
        private IEnumerable<Person> Persistent => new GetPeopleCommand().Run();

        public event EventHandler CanExecuteChanged = delegate { };

        public bool CanExecute(object parameter)
        {
            var target = ((IEnumerable<PersonViewModel>)parameter).Select(p => p.ToDomain());
            var equals = Enumerable.SequenceEqual(Persistent, target, EqualityComparer<Person>.Default);

            return !equals;
        }

        public void Execute(object parameter)
        {
            var target = ((IEnumerable<PersonViewModel>)parameter).Select(p => p.ToDomain());
            new UpdatePeopleCommand { Updated = target }.Run();

            ValidateExecutionState();
        }

        internal void ValidateExecutionState()
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }
    }
}
