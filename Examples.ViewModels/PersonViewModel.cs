﻿using Examples.Model.Domain;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Examples.ViewModels
{
    public class PersonViewModel : INotifyPropertyChanged
    {
        public PersonViewModel()
        {
        }

        public string FirstName
        {
            get => firstName;
            set
            {
                SetValue(ref firstName, value);
                NotifyChanged(nameof(FullName));
            }
        }
        private string firstName;

        public string LastName
        {
            get => lastName;
            set
            {
                SetValue(ref lastName, value);
                NotifyChanged(nameof(FullName));
            }
        }
        private string lastName;

        public string FullName => $"{FirstName} {LastName}";

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void SetValue<T>(ref T property, T value,
            [CallerMemberName] string propertyName = null)
        {
            property = value;
            NotifyChanged(propertyName);
        }

        private void NotifyChanged(string propertyName)
        {
            PropertyChanged(this,
                new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public PersonViewModel(Person person)
        {
            FirstName = person.FirstName;
            LastName = person.LastName;
            Departament = person.Departament;
            Email = person.Email;
        }

        public string Departament
        {
            get => departament;
            set => SetValue(ref departament, value);
        }
        private string departament;

        public string Email
        {
            get => email;
            set => SetValue(ref email, value);
        }
        private string email;

        internal Person ToDomain()
        {
            return new Person(FirstName, LastName, Email)
            {
                Departament = Departament
            };
        }
    }
}
